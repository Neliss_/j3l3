import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        arrayReading();
        System.out.println();
        stitchFiles();
        System.out.println();
        readingBook();


    }

    public static void arrayReading() {
        try (FileInputStream fin = new FileInputStream("C://Users//-Frezzed-//IdeaProjects//J3L3//123//test1.txt")) {
            byte[] buffer = new byte[fin.available()];
            fin.read(buffer, 0, buffer.length);
            for (int i = 0; i < buffer.length; i++) {
                System.out.print((char) buffer[i]);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void stitchFiles() {
        try {
            long a = System.currentTimeMillis();
            ArrayList<InputStream> ail = new ArrayList<>();
            ail.add(new FileInputStream("123/1/test1.txt"));
            ail.add(new FileInputStream("123/1/test2.txt"));
            ail.add(new FileInputStream("123/1/test3.txt"));
            ail.add(new FileInputStream("123/1/test4.txt"));
            ail.add(new FileInputStream("123/1/test5.txt"));

            FileOutputStream fos = new FileOutputStream("123/1/test.txt");
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            SequenceInputStream in = new SequenceInputStream(Collections.enumeration(ail));

            int x;
            while ((x = in.read()) != -1) {
                System.out.print((char) x);
                bos.write(x);
            }
            in.close();
            bos.close();
            System.out.println();
            System.out.println("Время на сшивку 5 файлов по 100 байт состаляет: " + (System.currentTimeMillis() - a));
        } catch (
                IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void readingBook() {
        int pageSize = 1800;
        try {
            RandomAccessFile raf = new RandomAccessFile("123/2/book.txt", "r");
            Scanner scanner = new Scanner(System.in);

            while (true) {
                System.out.println("Введите страницу от 0 до 254: ");
                int page = scanner.nextInt();
                raf.seek(page * pageSize);
                byte[] buffer = new byte[1800];
                raf.read(buffer);
                if (page <= 254) {
                    System.out.print(new String(buffer));
                    System.out.println();
                } else {
                    System.out.println("Книга закончилась!");
                }
            }
        } catch (
                IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
